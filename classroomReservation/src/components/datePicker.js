import React, { Component, useState } from "react";
import { View, Button } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";

const DateTimePicket = ({ type, buttonTitle, dateKey, setSchedule }) => {
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    if (type === "time") {
      const hour = date.getHours();
      const minute = date.getMinutes();

      const formattedTime = `${hour}:${minute}`;

      setSchedule((prevState) => ({
        ...prevState,
        [dateKey]: formattedTime,
      }));
    }else{
      const formattedDate = date.toISOString().split('T')[0];
    setSchedule((prevState) => ({
      ...prevState,
      [dateKey]: formattedDate,
    }));
    }
    hideDatePicker();
  };

  return (
    <View>
      <Button title={buttonTitle} onPress={showDatePicker} color="#fff" />
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode={type}
        locale="pt_BR"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
        pickerContainerStyleIOS={{ backgroundColor: "#fff" }}
        textColor="#000"
      />
    </View>
  );
};
export default DateTimePicket;
